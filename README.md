# MPower_App

### Intructiuni pentru instalare environment:

1. Verificare python in terminal: deschidere de terminal nou si scriere comanda python(sau py in functie de caz):
    - daca afiseaza vesiunea -> pasul 2
    - daca afiseaza eroare -> verifica daca ai instalat Python pe calculator si seteaza in environment variables path (cauta in casuta de windows si o sa ti dea ce ai nevoie) calea catre python si python/Scripts

2. Deschide un cmd si scrie comanda:
        
         python(sau py in functie de caz) -m pip install virtualenv (asta daca nu ai virtualenv)

3. Acum in VSCode deschide un terminal si scrie comanda: 
        
        python(sau py in functie de caz) -m virtualenv env (doar pentru setarea initiala)

4. In terminal scrie comanda: 
    
        env\Scripts\activate 
    
    Daca iti apare in terminal la inceput (env) - ai reusit sa activezi -> Cel mai probabil de fiecare data cand pornesti VSCode trebuie sa activezi env

5. Foloseste Ctrl+Shift+P si selecteaza Python:Select Interpreter -> Selecteaza Python-ul care se afla in fisierul env

6. Esti gata de lucru!  Felicitari!!!

---

### Instalare dependinte si conectare la baza de date

1. Dependinte:
        
        pip install -r requirements.txt
        pip install mysqlclient-1.4.2-cp37-cp37m-win32.whl
        pip install pymysql (in cazul in care aveti mysql 8, nu mysql 5.7)

2. Conectare

    1. Creati un folder landga folderul app care sa se numeasca instance
    2. In folderul instance creati un fisier config.py
    3. In fisierul instance/config.py scrieti urmatoarele:
        
        > SECRET_KEY = 'p9Bv<3Eid9%$i01' -> cheia poate fi ceva diferit
        >
        > SQLALCHEMY_DATABASE_URI = 'mysql://user:parola@localhost:port/nume_baza_de_date' (daca aveti mysql 5.7)
        >   
        > **sau**
        >   
        > SQLALCHEMY_DATABASE_URI = 'mysql://user:parola@localhost:port/nume_baza_de_date' (daca aveti mysql 8)

3. Comenzi initiale pentru setare environment

    ***E nevoie sa fie setat ca environmentul sa fie activat - adica sa apara in linia de comanda (env)***

    * Daca nu e activat environmentul atunci scrieti in linia de comanda: 
            
            env\scripts\activate

    * Setam variabilele de environment

        **Powershell:**
        
            $env:FLASK_CONFIG="development"
            $env:FLASK_APP="run.py"

        **Cmd:**

            set FLASK_CONFIG=development
            set FLASK_APP=run.py

    * Acum scrieti in cmd sau powershell comanda: 
        
             flask db upgrade

---

### Feature-uri

**O sa folosim &#10003; ca sa verificam progresul**

- Inregistrare &#10003; 
- Logare &#10003;
- Conversatie normala
- Conversatie de grup