from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from flask import json
from sqlalchemy.orm.attributes import QueryableAttribute
from sqlalchemy.sql.expression import not_

from marshmallow import post_load

import datetime

from app import db, login_manager, ma


class BaseModel(db.Model):
    __abstract__ = True

    def __init__(self, **kwargs):
        kwargs["_force"] = True
        self.from_dict(**kwargs)

    def to_dict(self, show=None, _hide=[], _path=None):

        show = show or []

        hidden = self._hidden_fields if hasattr(self, "_hidden_fields") else []
        default = self._default_fields if hasattr(
            self, "_default_fields") else []
        default.extend(["id"])

        if not _path:
            _path = self.__tablename__.lower()

            def prepend_path(item):
                item = item.lower()
                if item.split(".", 1)[0] == _path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != ".":
                    item = ".%s" % item
                item = "%s%s" % (_path, item)
                return item

            _hide[:] = [prepend_path(x) for x in _hide]
            show[:] = [prepend_path(x) for x in show]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        ret_data = {}

        for key in columns:
            if key.startswith("_"):
                continue

            check = "%s.%s" % (_path, key)

            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                ret_data[key] = getattr(self, key)

        for key in relationships:
            if key.startswith("_"):
                continue

            check = "%s.%s" % (_path, key)

            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                _hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    items = getattr(self, key)
                    if self.__mapper__.relationships[key].query_class is not None:
                        if hasattr(items, "all"):
                            items = items.all()

                    ret_data[key] = []
                    for item in items:
                        ret_data[key].append(
                            item.to_dict(
                                show=list(show),
                                _hide=list(_hide),
                                _path=("%s.%s" % (_path, key.lower())),
                            )
                        )
                else:
                    if (
                        self.__mapper__.relationships[key].query_class is not None
                        or self.__mapper__.relationships[key].instrument_class
                        is not None
                    ):

                        item = getattr(self, key)
                        if item is not None:
                            ret_data[key] = item.to_dict(
                                show=list(show),
                                _hide=list(_hide),
                                _path=("%s.%s" % (_path, key.lower())),
                            )
                        else:
                            ret_data[key] = None
                    else:
                        ret_data[key] = getattr(self, key)

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith("_"):
                continue
            if not hasattr(self.__class__, key):
                continue
            attr = getattr(self.__class__, key)
            if not (isinstance(attr, property) or isinstance(attr, QueryableAttribute)):
                continue

            check = "%s.%s" % (_path, key)
            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                val = getattr(self, key)
                if hasattr(val, "to_dict"):
                    ret_data[key] = val.to_dict(
                        show=list(show),
                        _hide=list(_hide),
                        _path=("%s.%s" % (_path, key.lower())),
                    )
                else:
                    try:
                        ret_data[key] = json.loads(json.dumps(val))
                    except:
                        pass

        return ret_data

    def from_dict(self, **kwargs):
        """Update this model with a dictionary."""

        _force = kwargs.pop("_force", False)

        readonly = self._readonly_fields if hasattr(
            self, "_readonly_fields") else []
        if hasattr(self, "_hidden_fields"):
            readonly += self._hidden_fields

        readonly += ["id"]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        changes = {}

        for key in columns:
            if key.startswith("_"):
                continue
            allowed = True if _force or key not in readonly else False
            exists = True if key in kwargs else False
            if allowed and exists:
                val = getattr(self, key)
                if val != kwargs[key]:
                    changes[key] = {"old": val, "new": kwargs[key]}
                    setattr(self, key, kwargs[key])

        for rel in relationships:
            if key.startswith("_"):
                continue
            allowed = True if _force or rel not in readonly else False
            exists = True if rel in kwargs else False
            if allowed and exists:
                is_list = self.__mapper__.relationships[rel].uselist
                if is_list:
                    valid_ids = []
                    query = getattr(self, rel)
                    cls = self.__mapper__.relationships[rel].argument()
                    for item in kwargs[rel]:
                        if (
                            "id" in item
                            and query.filter_by(id=item["id"]).limit(1).count() == 1
                        ):
                            obj = cls.query.filter_by(id=item["id"]).first()
                            col_changes = obj.from_dict(**item)
                            if col_changes:
                                col_changes["id"] = str(item["id"])
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(item["id"]))
                        else:
                            col = cls()
                            col_changes = col.from_dict(**item)
                            query.append(col)
                            db.session.flush()
                            if col_changes:
                                col_changes["id"] = str(col.id)
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(col.id))

                    # delete rows from relationship that were not in kwargs[rel]
                    for item in query.filter(not_(cls.id.in_(valid_ids))).all():
                        col_changes = {"id": str(item.id), "deleted": True}
                        if rel in changes:
                            changes[rel].append(col_changes)
                        else:
                            changes.update({rel: [col_changes]})
                        db.session.delete(item)

                else:
                    val = getattr(self, rel)
                    if self.__mapper__.relationships[rel].query_class is not None:
                        if val is not None:
                            col_changes = val.from_dict(**kwargs[rel])
                            if col_changes:
                                changes.update({rel: col_changes})
                    else:
                        if val != kwargs[rel]:
                            setattr(self, rel, kwargs[rel])
                            changes[rel] = {"old": val, "new": kwargs[rel]}

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith("_"):
                continue
            allowed = True if _force or key not in readonly else False
            exists = True if key in kwargs else False
            if allowed and exists and getattr(self.__class__, key).fset is not None:
                val = getattr(self, key)
                if hasattr(val, "to_dict"):
                    val = val.to_dict()
                changes[key] = {"old": val, "new": kwargs[key]}
                setattr(self, key, kwargs[key])

        return changes


# Tabelele normale acum
user_group = db.Table(
    "user_group",
    db.Model.metadata,
    db.Column("group_id", db.BigInteger,
              db.ForeignKey("group_conversations.id")),
    db.Column("user_id", db.BigInteger, db.ForeignKey("users.id")),
)


class User(UserMixin, BaseModel):
    """
    Create an Users table
    """

    __tablename__ = "users"

    id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    logged_in = db.Column(db.Boolean)
    messages = db.relationship(
        "Message", backref="user", lazy="dynamic", uselist=True)
    groups = db.relationship(
        "GroupConversation",
        secondary=user_group,
        lazy="dynamic",
        back_populates="users",
    )

    _default_fields = ["id", "email", "username", "logged_in", "groups"]

    @property
    def password(self):
        """
        Prevent password from being accessed
        """
        raise AttributeError("password is not a readable attribute.")

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User: {}>".format(self.username)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, User):
            return self.id == other.id
        return False




# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Conversation(BaseModel):
    """
    Create an Conversation table
    """
    __tablename__ = "conversations"

    __table_args__ = (
        db.UniqueConstraint("user1_id", "user2_id", name="unique_user_pair"),
    )

    id = db.Column(db.BigInteger, primary_key=True)
    user1_id = db.Column(db.BigInteger, db.ForeignKey("users.id"))
    user2_id = db.Column(db.BigInteger, db.ForeignKey("users.id"))

    user1 = db.relationship("User", foreign_keys=[user1_id])
    user2 = db.relationship("User", foreign_keys=[user2_id])

    status = db.Column(db.String(60), default="invitation")
    messages = db.relationship("Message")

    _default_fields = ["id", "user1", "user2", "status", "messages"]




class Message(BaseModel):
    """
    Create an Message table
    """

    __tablename__ = "messages"

    id = db.Column(db.BigInteger, primary_key=True)
    reply = db.Column(db.String(256))
    time = db.Column(db.DateTime, default=datetime.datetime.now())
    status = db.Column(db.String(60), default="sending")
    user_id = db.Column(db.BigInteger, db.ForeignKey("users.id"))
    conversation_id = db.Column(
        db.BigInteger, db.ForeignKey("conversations.id"))
    group_id = db.Column(
        db.BigInteger, db.ForeignKey("group_conversations.id"))

    _default_fields = ["id", "reply", "status", "time"]



class GroupConversation(BaseModel):
    """
    Create an GroupConversation table
    """

    __tablename__ = "group_conversations"

    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String(60), default="Default_Group")
    status = db.Column(db.String(60), default="initiated")
    users = db.relationship("User", secondary=user_group,
                            back_populates="groups")


class Friends(BaseModel):
    __tablename__ = "friends"

    user1_id = db.Column(db.BigInteger, db.ForeignKey(
        "users.id"), primary_key=True)
    user2_id = db.Column(db.BigInteger, db.ForeignKey(
        "users.id"), primary_key=True)

    user1 = db.relationship("User", foreign_keys=[user1_id])
    user2 = db.relationship("User", foreign_keys=[user2_id])

    _default_fields = ["user1", "user2"]

    def __repr__(self):
        return "<Friends: {}>".format(self.user1.username + "_" + self.user2.username)

class MessageSchema(ma.Schema):
    class Meta:
        fields = ('id', 'reply', 'status', 'time','user_id')

    @post_load
    def make_message(self, data):
        return Message(**data)


class UserSchema(ma.Schema):
    messages=ma.Nested(MessageSchema,many=True)
    class Meta:
        fields = ("id", "email", "username", "logged_in")
    
    @post_load
    def make_user(self, data):
        return User(**data)


class ConversationSchema(ma.Schema):
    messages=ma.Nested(MessageSchema,many=True)
    user1=ma.Nested(UserSchema)
    user2=ma.Nested(UserSchema)
    class Meta:
        fields = ('id', 'user1', 'user2', 'status', 'messages')

    @post_load
    def make_conversation(self, data):
        return Conversation(**data)

class FriendseSchema(ma.Schema):
    user1=ma.Nested(UserSchema)
    user2=ma.Nested(UserSchema)

    class Meta:
        fields = ('user1', 'user2')

    @post_load
    def make_friends(self, data):
        return Friends(**data)
