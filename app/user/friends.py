from ..models import Friends
from ..models import User
from flask_login import current_user
from flask import request
from flask.json import jsonify
from app import db

from . import user

import json

@user.route("/api/friends", methods=['GET'])
def get_all_Friends():
    id_user= -1
    if request.args.get('id'):
        id_user = int(request.args.get('id')) 
    else: 
        id_user = current_user.id 
    
    user_friends= [friend.to_dict() for friend in getfriends(id_user)]
    
    return jsonify(user_friends)

def getfriends(id_user):
    friends= Friends.query.filter((Friends.user1_id==id_user) | (Friends.user2_id==id_user)).all()
    user_friends=[]
    for friend in friends:
        print(friend.user1_id)
        print(friend.user2_id)
        if friend.user1_id == id_user:
            user_friends.append(friend.user2)
        else:
            user_friends.append(friend.user1)
    return user_friends

@user.route("/api/nonfriends", methods=['GET'])
def get_all_users_not_friends():
    id_user= -1
    if request.args.get('id'):
        id_user = int(request.args.get('id')) 
    else: 
        id_user = current_user.id 

    users = User.query.all()

    friends = getfriends(id_user)

    for friend in friends:
        users.remove(friend)
    for user in users:
        if(user.id == id_user):
            users.remove(user)
    return jsonify([user.to_dict() for user in users])

@user.route("/api/friends", methods=['POST'])
def addFriend():
    friends=request.get_json() or request.form
    friend_db=Friends(user1_id=friends['id_user'],user2_id=friends['id_friend'])
    db.session.add(friend_db)
    db.session.commit()
    db.session.refresh(friend_db)
    return jsonify(friend_db.to_dict()),201



