from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired



class MessageForm(FlaskForm):
    """
    Form for users to send message
    """
    message = StringField(validators=[DataRequired()])
    submit = SubmitField()