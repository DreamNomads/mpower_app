from ..models import GroupConversation
from flask_login import current_user
from ..models import user_group
from app import db

from datetime import datetime


def get_all_groups():
    return current_user.groups

def get_messages_by_group(grup):
    messages= Message.query.filter(Message.group_id==grup.id).all()
    return messages

def create_group(group_name, users_for_group):
    users_for_group.append(current_user)
    group=GroupConversation(status="aproved",name=group_name)
    db.session.add(group)
    group=GroupConversation.query.filter_by(name=group_name).first()
    for user in users_for_group:
        group.users.append(user)
    db.session.commit()

def send_message_in_group(group,message):
    db.session.add(Message(reply=message, time=datetime.now, status="sent", user_id=current_user.id, group_id=group.id))
    db.session.commit()
