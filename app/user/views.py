from flask import abort, render_template, request, url_for, redirect
from flask_login import current_user, login_required

import requests

from . import user
from .forms import MessageForm
from ..models import User, Conversation, Message, GroupConversation, MessageSchema
from .friends import get_all_Friends, get_all_users_not_friends, addFriend
from .conversations import get_all_conversations, get_conversation, get_messages_by_conversation, create_conversation, accept_conversation, delete_conversation, send_message_to_conversation

import json

messages_schema=MessageSchema(many=True)

@user.route('/chat/<int:uid>')
def chat(uid):
    """
    Render the homepage template on the /chat route
    """
    form = MessageForm()
    conversations = get_all_conversations().get_json()

    active_conversation = -1

    if len(conversations) > 0:
        active_conversation = conversations[0]

        return redirect(url_for('user.chat_with', uid=uid, conversation_id=active_conversation['id']))

    else:
        return render_template('user/chat.html', title="Chat", username=User.query.get_or_404(uid).username,
                               conversations=[], messages=[], current_conversation=None,
                               form=form)


@user.route('/chat/<int:uid>/c/<int:conversation_id>', methods=['GET'])
def chat_with(uid, conversation_id):
    form = MessageForm()
    conversations = get_all_conversations().get_json()
    active_conversation = get_conversation(conversation_id).get_json()

    return render_template('user/chat.html', title="Chat",
                           username=User.query.get_or_404(uid).username,
                           conversations=conversations, current_conversation=active_conversation,
                           form=form)

@user.route('/conversations/<int:conversation_id>/messages')
def render_conversation_messages(conversation_id):
    active_conversation = get_conversation(conversation_id).get_json()
    messages = get_messages_by_conversation(
        conversation_id).get_json()

    messages_model = messages_schema.load(messages).data
    return render_template('user/chathistory.html',messages=messages_model,current_conversation=active_conversation)