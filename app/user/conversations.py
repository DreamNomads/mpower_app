from ..models import Conversation, ConversationSchema
from ..models import Message, MessageSchema
from flask_login import current_user
from flask.json import jsonify
from flask import request, Response
from sqlalchemy import desc

from datetime import datetime
from app import db

from . import user

import json

message_schema = MessageSchema()
messages_schema = MessageSchema(many=True)
conversation_schema = ConversationSchema()
conversations_schema = ConversationSchema(many=True)


@user.route("/api/conversations", methods=['GET'])
def get_all_conversations():
    id_user = -1
    if request.args.get('id'):
        id_user = int(request.args.get('id'))
    else:
        id_user = current_user.id
    conversations = Conversation.query.filter(
        (Conversation.user1_id == id_user) | (Conversation.user2_id == id_user)).all()
    result = conversations_schema.dump(conversations)
    return jsonify(result.data)


@user.route("/api/conversations/<int:cid>", methods=['GET'])
def get_conversation(cid):
    id_user = -1
    if request.args.get('id'):
        id_user = int(request.args.get('id'))
    else:
        id_user = current_user.id
    conversations = Conversation.query.filter(((Conversation.user1_id == id_user) | (
        Conversation.user2_id == id_user)) & Conversation.id == cid).first()
    result = conversation_schema.dump(conversations)
    return jsonify(result.data)


@user.route("/api/conversations/<int:id_conv>/messages", methods=['GET'])
def get_messages_by_conversation(id_conv):
    messages = Message.query.filter(Message.conversation_id == id_conv).order_by(Message.time).all()
    result = messages_schema.dump(messages)
    return jsonify(result.data)


@user.route("/api/conversations", methods=['POST'])
def create_conversation():
    users = request.get_json() or request.form
    id_current_user = int(users['id_current_user']) or current_user.id
    id_user = int(users['id_user'])

    conv_db = Conversation(user1_id=id_current_user,
                           user2_id=id_user, status="invitation")
    try:
        db.session.add(conv_db)
        db.session.commit()
        db.session.refresh(conv_db)

        return Response(response=json.dumps(conversation_schema.dump(conv_db).data), status=201)
    except:
        return Response(response="Conversation already exists", status=400)


@user.route("/api/conversations", methods=['PUT'])
def accept_conversation():
    conversation = request.get_json() or request.form
    id_conv = int(conversation['id_conversation'])

    conv = Conversation.query.get_or_404(id_conv)

    update = {
        "status": "active"
    }

    try:
        conv.status = "active"
        db.session.commit()

        return Response(response=json.dumps(conversation_schema.dump(conv).data), status=200)
    except:
        return Response(response="Cannot do it", status=400)


@user.route("/api/conversations/<int:id_conv>", methods=['DELETE'])
def delete_conversation(id_conv):
    conv = Conversation.query.get_or_404(id_conv)

    db.session.delete(conv)
    db.session.commit()

    return Response(response="Deleted conversation", status=200)


@user.route("/api/conversations/<int:id_conv>/messages", methods=['POST'])
def send_message_to_conversation(id_conv):
    conversation=None
    id_current_user=-1

    if request.is_json:
        conversation = request.get_json()
    else:
        conversation =request.form
    
    id_current_user = int(conversation['id_current_user']) 
    
    message = conversation['message']

    message_db = Message(reply=message, time=datetime.now().strftime('%Y-%m-%d %H:%M:%S'), status="sent",
                         user_id=id_current_user, conversation_id=id_conv)
    db.session.add(message_db)
    db.session.commit()
    db.session.refresh(message_db)

    print(message_db)

    return Response(response=json.dumps(message_schema.dump(message_db).data), status=201)
