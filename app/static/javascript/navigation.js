$(document).ready(function () {
    $('#link-convs').click(function () {
        $('#list-tabs').find('li.nav-item a.active').removeClass('active');
        $('#content-holder').find('div.active-content').removeClass('active-content');
        $('#list-convs').addClass('active-content');
        $(this).addClass('active');
    });
    $('#link-friends').click(function () {
        $('#list-tabs').find('li.nav-item a.active').removeClass('active');
        $('#content-holder').find('div.active-content').removeClass('active-content');
        $('#list-friends').addClass('active-content');
        $(this).addClass('active');
    });
    $('#link-groups').click(function () {
        $('#list-tabs').find('li.nav-item a.active').removeClass('active');
        $('#content-holder').find('div.active-content').removeClass('active-content');
        $('#list-groups').addClass('active-content');
        $(this).addClass('active');
    });

    
});