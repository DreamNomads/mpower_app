from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_marshmallow import Marshmallow

from config import app_config

from datetime import datetime

db = SQLAlchemy()
ma = Marshmallow()

login_manager = LoginManager()

app = Flask(__name__)

def format_datetime(value, format="%I:%M %p   %d %b %Y"):
    if value is None:
        return ""
    date= datetime.fromisoformat(value)
    return date.strftime(format)

def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    Bootstrap(app)
    db.init_app(app)
    ma.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)

    from app import models

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    from .user import user as user_blueprint
    app.register_blueprint(user_blueprint)

    app.jinja_env.filters['formatdatetime'] = format_datetime

    return app

