import os

from app import create_app
from flask_socketio import SocketIO
import requests
from flask import url_for

from app.user.conversations import send_message_to_conversation

config_name = os.getenv('FLASK_CONFIG')
app = create_app(config_name)
socketio=SocketIO(app)

def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')

@socketio.on('connect-client')
def handle_connection(json, methods=['GET', 'POST']):
    print(str(json))

@socketio.on('send-message')
def handle_send_message(json, methods=['GET', 'POST']):
    print('received my event: ' + str(json))
    api_url=url_for('user.send_message_to_conversation',id_conv=int(json['id_conv']), _external=True)
    print(api_url)
    r = requests.post(url=api_url,json=json)
    print(r.text)
    socketio.emit('refresh-messages', r.status_code, callback=messageReceived)

if __name__ == '__main__':
    socketio.run(app,threaded=True)